import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import news from './news';
import product from "../Moudels/product";
import {ProductService} from "../service/product.service";
import {Router} from "@angular/router";
import {BlogService} from "../service/blog.service";
import blog from "../Moudels/blog";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class HomeComponent implements OnInit {
  list: Array<news>;
  sList: Array<product>;
  index: number;
  slides: any = [[]];
  slides2: any = [[]];

  constructor(private router: Router ,private service: ProductService, private blog_service: BlogService) {}


  ngOnInit() {
    /*
    this.service.getProduct().subscribe( res => {
      this.sList = res;
      console.log(res);
    },
      error => {
        this.router.navigate(['../!**']);
        console.log(error);
      });

  */

    let cards = [];
    let temp = [];
    this.blog_service.getEntries().subscribe(
      (res:any) =>{
        for(let num:number =0 ;num < res.count; num++){
          let obj = {
            title: res.entries[num].title,
            description: res.entries[num].description,
            img: res.entries[num].image
          };
          temp.push(obj);
        }
      } ,
      (error:HttpErrorResponse) => {
        console.log(error);
      });
    let obj = {
      title: 'salam',
      description: 'salam man be to yar ghadimi',
      img: ''
    };
    cards.push(obj);
    console.log(cards);
    console.log(temp);
    cards.push(temp);
    console.log(cards[1]);
    this.sList = this.service.fake();
    this.slides2 = this.chunk(this.sList , 4);
    this.slides = this.chunk(cards[1] , 4);
  }

  chunk(arr: Array<any>, chunkSize) {
    let R = [];
    console.log(arr.length);
    for (let i = 0, len = arr.length; i < len; i += chunkSize) {
      R.push(arr.slice(i, i + chunkSize));
    }
    return R;
  }

}
