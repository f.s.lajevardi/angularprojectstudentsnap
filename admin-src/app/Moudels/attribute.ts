export default class attribute {
  name: string;
  detail: string;
  order: number;
  isChecked: boolean;
  constructor() {
    this.isChecked = false;
  }
}
