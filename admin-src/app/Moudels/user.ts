export default class user {
  constructor() {
  }
  username: string;
  email: string;
  first_name: string;
  last_name: string;
  gender: string;
  national_code: string;
  mobile_number: string;
  date_birth: string;
  group : string;
  ///admin - blogger - user - seller
}
