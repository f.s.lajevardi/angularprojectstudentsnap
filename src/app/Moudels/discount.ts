export default class discount {
  title: string;
  group : string;
  percent: number;
  start_date: string;
  end_date: string;
}
