import { Component, OnInit } from '@angular/core';
import {ManagerService} from "../services/manager.service";
import {Observable} from "rxjs";
import {stringify} from "querystring";
import user from "../Moudels/user";

@Component({
  selector: 'app-manager-component',
  templateUrl: './manager-component.component.html',
  styleUrls: ['./manager-component.component.css']
})
export class ManagerComponentComponent implements OnInit {
  result :Array<user>;
  updatedUser :user;

  constructor(private backend: ManagerService) {
    this.updatedUser = new user();
    //backend.updateUser(this.updatedUser , 'https://api.anooshabad.com/auth/user/');

    this.result = backend.get_userList('https://api.anooshabad.com/auth/users/0/');

  }

  ngOnInit() {
  }

  editButton(){
    this.backend.updateUser(this.updatedUser).subscribe(res=>{
      console.log('res'+res);
    },
      error=>{
        console.log('error'+error);
      });
  }

  openEditForm(user: user){
    document.getElementById('edit_form').style.display = 'block';
    this.updatedUser = user;

  }

  closeButton(){
    document.getElementById('edit_form').style.display = 'none';
  }

}
