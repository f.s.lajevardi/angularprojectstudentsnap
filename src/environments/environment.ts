// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  createCartUrl: 'https://api.anooshabad.com/store/cart/',
  updateCartUrl: 'https://api.anooshabad.com/store/cart/',
  delCartUrl: 'https://api.anooshabad.com/store/cart/',
  getCart: 'https://api.anooshabad.com/store/cart/',
  getEntries: 'https://api.anooshabad.com/blog/entries/1',
  getProductById: 'https://api.anooshabad.com/store/product',
  getProduct: 'https://api.anooshabad.com/store/products/1/',
  register: 'https://api.anooshabad.com/rest-auth/registration/',
  login: 'https://api.anooshabad.com/rest-auth/login/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
