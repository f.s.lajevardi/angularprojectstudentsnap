import { Component, OnInit } from '@angular/core';
import {ManagerService} from "../services/manager.service";
import discount from "../Moudels/discount";

@Component({
  selector: 'app-discount',
  templateUrl: './discount.component.html',
  styleUrls: ['./discount.component.css']
})

export class DiscountComponent implements OnInit {

  newDiscount: discount;
  disList: Array<discount>;
  constructor(private service: ManagerService) {
    this.newDiscount = new discount();
    this.disList = new Array<discount>();
  }

  createDiscount(){
    this.service.createDiscount(this.newDiscount).subscribe(res=>{
      console.log(res);
    }, error => {
      window.alert('خطا در ایجاد تخفیف جدید');
    });
  }

  deleteDiscount(obj: any){
    this.service.deleteCategory(obj.uuid).subscribe(res=>{
      console.log(res);
      },error => {
      window.alert('خطا در حذف تخفیف');
    })
  }

  ngOnInit() {
    this.service.getDiscount().subscribe(
      (res:any) =>{
        console.log(res);
        this.disList = res.data;
      },
      error => {
        console.log(error);
        window.alert('خطا در بار گزاری لیست تخفیف ها');
      });
  }

}
