export default class blog {
  title: string;
  slug: string;
  description: string;
  available: boolean;
}
