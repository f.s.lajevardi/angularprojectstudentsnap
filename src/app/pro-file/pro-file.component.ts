import { Component, OnInit } from '@angular/core';
import OrderService from "./OrderService";
import Order from "./Order";

@Component({
  selector: 'app-pro-file',
  templateUrl: './pro-file.component.html',
  styleUrls: ['./pro-file.component.css']
})
export class ProFileComponent implements OnInit {

  listOrders: Array<Order>;
  phone: string;
  gender: string;
  age: string;
  personalcode: string;

  constructor() {
    const service_order = new OrderService();
    this.listOrders = service_order.getList();

    this.gender = 'ثبت نشده';
    this.phone = 'ثبت نشده';
    this.personalcode= 'ثبت نشده';
    this.age = 'ثبت نشده';
  }
  ngOnInit() {
  }

  openForm(){
    document.getElementById('moreInfo').style.display= 'block';
  }

  close(){
    document.getElementById('moreInfo').style.display = 'none';
  }

  subnite_edite(input){
    this.gender = input.value;
  }
}

