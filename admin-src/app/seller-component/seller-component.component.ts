import { Component, OnInit } from '@angular/core';
import {SellerService} from "../services/seller.service";
import {logger} from "codelyzer/util/logger";

@Component({
  selector: 'app-seller-component',
  templateUrl: './seller-component.component.html',
  styleUrls: ['./seller-component.component.css']
})
export class SellerComponentComponent implements OnInit {

  selected_img: File = null;
  constructor(private service: SellerService) {
    service.getProductList('https://api.anooshabad.com/store/products/0/');
  }

  ngOnInit() {
  }

  open_form(id){
    document.getElementById(id).style.display = 'block';
  }

  close_form(id){
    document.getElementById(id).style.display = 'none';
  }

  onFileSelected(event){
    this.selected_img = <File>event.target.files[0];
    ///send to server
  }

  upLoad(){

  }
}
