import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import product from "../Moudels/product";
import {Observable, throwError} from "rxjs";
import {catchError, retry} from "rxjs/operators";
import {environment} from "../../environments/environment";
import {JwtService} from "./jwt.service";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient , private jwt: JwtService) {
  }

  getAttribute(url :string) :Observable<any> {
    const httpOptions = {
      headers : this.jwt.getHeader()
    };

    return this.http.get<any[]>(url , httpOptions);
  }

  getProductById(uuid: string) :Observable<any>{
    const httpOptions = {
      headers : this.jwt.getHeader()
    };
    return this.http.get<any>(environment.getProductById+'/'+ uuid + '/',httpOptions).pipe(retry(3));
  }

  getProduct() :Observable<any> {

    const httpOptions = {
      headers : this.jwt.getHeader()
    };

    return this.http.get<product[]>(environment.getProductById , httpOptions).pipe(retry(3));
  }

  errorHandler(error :HttpErrorResponse){
    return throwError(error.status);
  }














  fake(): Array<product>{
    var productList = new Array<product>();
    let obj :product = new product();
    obj.description = 'سانی که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک برنامه از ';
    obj.sale_price=900;
    obj.uuid = 'gLEfyfuoCSLEWV6wR9dgwJ';
    obj.is_hot = false;
    obj.is_available = true;
    obj.slug = 'uihiuhyug';
    obj.title= 'پذیرایی دفاع';
    obj.discount = null;
    obj.quantity = 55;
    obj.category = '';
    obj.image_url = 'https://mehmano.com/img/uploads/20191102110243_5dbd62533987f.png';
    obj.properties = null;
    productList.push(obj);
    productList.push(obj);
    productList.push(obj);
    return productList;
  }
}
