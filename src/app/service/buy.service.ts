import { Injectable } from '@angular/core';
import {HttpClient , HttpHeaders} from "@angular/common/http";
import {JwtService} from "./jwt.service";
import {environment} from "../../environments/environment";
import cart from "../Moudels/cart";
import {retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class BuyService {

  constructor(private http: HttpClient ,private jwt: JwtService) {

  }

  createCart(){
    let test: Array <cart> = [];//example!!!!!
    let obj = {
      products: test
    };

    console.log(JSON.stringify(obj));
    let httpOptions = {
      headers: this.jwt.getHeader()
    };
    return this.http.post(environment.createCartUrl , JSON.stringify(test) , httpOptions).pipe(retry(3));
  }

  updateCart(productList: Array<cart>) {
    let obj = {
      products: productList
    };
    let httpOptions = {
      headers: this.jwt.getHeader()
    };
    let test = JSON.stringify(obj);
    console.log(test);
    return this.http.put<any>(environment.updateCartUrl , test , httpOptions).pipe(retry(3));
  }

  getCart(){
    let httpOptions = {
      headers: this.jwt.getHeader()
    };
    return this.http.get(environment.getCart , httpOptions).pipe(retry(3));
  }

  delCart () {

  }
}
