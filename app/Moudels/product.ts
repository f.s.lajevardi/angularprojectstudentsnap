import attribute from './attribute'
import discount from "./discount";

export default class product {
  constructor() {
  }

  title: string;
  slug: string;
  description: any;
  is_available: boolean;
  is_hot: boolean;
  uuid: string;
  image_url: string;
  category: string;
  quantity: number;
  sale_price: number;
  properties :attribute[];
  discount: discount;
}
