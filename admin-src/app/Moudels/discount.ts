export default class discount {
  title: string;
  group : any;
  percent: number;
  is_public_title: boolean;
  start_date: string;
  end_date: string;

  constructor() {
    this.group = {
      name: "general"
    };

    this.is_public_title = true;
  }
}
