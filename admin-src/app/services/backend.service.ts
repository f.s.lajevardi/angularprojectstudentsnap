import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {tap, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  constructor(private httpClient: HttpClient) {}

  getHeader() :any{
    const headers_object = new HttpHeaders().set("Authorization", "JWT " + localStorage.getItem('access_token')).append('Content-Type', 'application/json');
    return {
      headers: headers_object
    };
  }

  register(_username:string, _email:string, _password1:string , _password2: string) {
    const user = {
      username: _username,
      email: _email,
      password1: _password1,
      password2: _password2
    };
    const userjson = JSON.stringify(user);
    console.log(userjson);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    const options = { headers };
    return this.httpClient.post<{access_token: string}>('https://api.anooshabad.com/rest-auth/registration/',userjson , options).pipe(tap(res => {
      this.login(_username,_password1)
    }))
  }

  login(usernam: string,  passwor: string) {
    const user = {
      username: usernam,
      password: passwor
    };
    const userjson = JSON.stringify(user);
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    const options = { headers };

    console.log(userjson);
    return this.httpClient.post<any>('https://api.anooshabad.com/rest-auth/login/', userjson, options)
      .pipe(map(res => {
        console.log('login: ', res);
        localStorage.setItem('access_token', res.token);
        localStorage.setItem('currentUser', res.user);
      }));
  }

  public get loggedIn(): boolean {
    return localStorage.getItem('access_token') !==  null;
  }

  public error_handel( err :HttpErrorResponse){
    if(err.status == 400){
    }
  }

  public logout(){
    localStorage.removeItem('access_token');
  }

}
