// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  updateUser: 'https://api.anooshabad.com/auth/user/',
  getAttribute: 'https://api.anooshabad.com/store/attributes/1/',
  createAttribute: 'https://api.anooshabad.com/store/attribute/',
  createProduct: 'https://api.anooshabad.com/store/product/',
  createCategory: 'https://api.anooshabad.com/store/category/',
  getCategory: 'https://api.anooshabad.com/store/categories/1/',
  getDiscount: 'https://api.anooshabad.com/store/discounts/1/',
  createDiscount: 'https://api.anooshabad.com/store/discount/',
  upLoudImage: 'https://api.anooshabad.com/store/image/',
  getEntry: 'https://api.anooshabad.com/blog/entries/1',
  createEntry: 'https://api.anooshabad.com/blog/entry/',
  uploadEntryImage: 'https://api.anooshabad.com/blog/image/'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
