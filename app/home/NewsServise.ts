import news from './news';

export default class NewsServise {
  private newsLis: Array<news> = [];
  constructor() {
  }
  public getList() {
    this.newsLis = [
      {
        id: 1,
        // tslint:disable-next-line:max-line-length
        detail: 'کسانی که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک برنامه از سورس‌کد آن اس,',
        imgUrl: 'https://article.innovadatabase.com/articleimgs/article_images/636995556375716447AdobeStock_76774066.jpeg'
      },
      {
        id: 2,
        detail: 'بل از هرچیز لازم است مواردی را نصب کنید. برای این‌کار نسخه‌ی متناسب با سیستم‌عامل خود را از اینجا دانلود و نصب کنید',
        imgUrl: 'https://image.redbull.com/rbcom/010/2015-07-27/1331737542701_2/0010/1/1150/1438/1/moon-hill-natural-bridge-in-china.jpg'
      },
      {
        id: 3,
        // tslint:disable-next-line:max-line-length
        detail: 'تنظیمات دیگری مانند تغییر نام و ایمیل و چگونگی نمایش پیام کامیت نیز قابل انجام است. ما در این آموزش از vim به‌عنوان ادیتور استفاده می‌کنیم؛ اما شما می‌توانید انتخاب متفاوت خود را داشته باشید.',
        imgUrl: 'https://www.rehahnphotographer.com/shop/466-large_default/natural-umbrella.jpg'
      }
    ];
    return this.newsLis;
  }

  setList(newList: Array<news>) {
    /*
        push data to sever
    */
  }
}

