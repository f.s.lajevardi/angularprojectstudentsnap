import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogerComponentComponent } from './bloger-component.component';

describe('BlogerComponentComponent', () => {
  let component: BlogerComponentComponent;
  let fixture: ComponentFixture<BlogerComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogerComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogerComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
