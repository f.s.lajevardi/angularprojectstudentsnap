import { Component } from '@angular/core';
import {BackendService} from "./services/backend.service";
import {catchError, first, map} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  navbarOpen = false;
  isLogin: boolean;

  username = "";
  password = "";
  repit_password = "";
  phone_ = "";
  active_code = "";
  email="";

  passwordClass;
  usernameClass;
  emptyClass;
  repit_passClass;
  activeClass;
  server_error_class;
  wrong_user_class;

  server_error= "خطا در ثبت اطلاعات";
  wrong_user="رمز عبور یا نام کاربری نامعتبر";

  repet_passW_error = "تکرار پسورد نامعتبر می باشد";
  password_error = "پسورد حداقل 6 کاراکتر باید باشد";
  username_error = "نام کاربری حداقل 3 کارکتر باید باشد";
  empty_fild = "تمام فیلد ها را کامل کنید";

  constructor(private service : BackendService) {
    this.isLogin = this.service.loggedIn;
    this.passwordClass = 'error-text2';
    this.usernameClass = 'error-text2';
    this.emptyClass = 'error-text2';
    this.repit_passClass = 'error-text2';
    this.activeClass = 'error-text2';
    this.server_error_class = 'error-text2';
    this.wrong_user_class = 'error-text2';
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  loginButton(){
    this.passwordClass = 'error-text2';
    this.usernameClass = 'error-text2';
    this.emptyClass = 'error-text2';

    if(this.username =="" || this.password ==""){
      this.emptyClass = 'error-text1'
    }
    else if(this.username.length< 3 && this.password.length < 6){
      this.passwordClass = 'error-text1';
      this.usernameClass = 'error-text1';
    }
    else if(this.username.length>= 3 && this.password.length < 6){
      this.passwordClass = 'error-text1';
      this.usernameClass = 'error-text2';
    }
    else if(this.username.length < 3 && this.password.length >= 6){
      this.passwordClass = 'error-text2';
      this.usernameClass = 'error-text1';
    }
    else{
      this.service.login(this.username , this.password).pipe(first())
        .subscribe(
          data => {
//            console.log(data);
            location.reload();
            this.server_error_class = 'error-text2';
            this.wrong_user_class = 'error-text2';
            window.alert('عزیز خوش آمدی'+this.username);

          },
          error => {
            this.service.error_handel(error);
            this.username ='';
            this.password='';
            this.wrong_user_class = 'error-text1';
          }
        );
    }
  }

  registerButton(){
    this.passwordClass = 'error-text2';
    this.usernameClass = 'error-text2';
    this.emptyClass = 'error-text2';
    this.repit_passClass = 'error-text2';

    if(this.username == "" || this.password == "" || this.repit_password == ""){
      this.emptyClass = 'error-text1';
    }

    else if(this.username.length< 3 && this.password.length < 6){
      this.passwordClass = 'error-text1';
      this.repit_passClass = 'error-text2';
      this.usernameClass = 'error-text1';
    }
    else if(this.username.length>= 3 && this.password.length < 6 ){
      this.passwordClass = 'error-text1';
      this.usernameClass = 'error-text2';
      this.repit_passClass = 'error-text2';

    }
    else if(this.username.length < 3 && this.password.length >= 6 ){
      this.passwordClass = 'error-text2';
      this.usernameClass = 'error-text1';
      this.repit_passClass = 'error-text2';
    }

    else if(this.username.length >= 3 && this.password.length >= 6 ){
      if(this.password == this.repit_password){
        this.passwordClass = 'error-text2';
        this.usernameClass = 'error-text2';
        this.repit_passClass = 'error-text2';
        console.log("registraoi");
        this.service.register(this.username , this.email , this.password , this.repit_password).pipe(first())
          .subscribe(
            data =>{
              location.reload();
              this.server_error_class = 'error-text2';
              window.alert('اطلاعات شما با موفقیت ثبت شد');
            },
            error => {
              this.server_error_class = 'error-text1';
              this.username ='';
              this.password = '';
              this.repit_password = '';
              this.email = '';
              this.service.error_handel(error);
            });
      }
      else{
        this.repit_passClass = 'error-text1';
      }
    }
  }

  closeButton(){
    this.username = '';
    this.password = '';
    this.phone_ = '';
    this.repit_password = '';
    this.email = '';
    this.active_code = '';
    this.passwordClass = 'error-text2';
    this.usernameClass = 'error-text2';
    this.emptyClass = 'error-text2';
    this.repit_passClass = 'error-text2';
    this.activeClass = 'error-text2';
    this.server_error_class = 'error-text2';
    this.wrong_user_class = 'error-text2';

    document.getElementById('id01').style.display = 'none';
    document.getElementById('id02').style.display = 'none';
    document.getElementById('id03').style.display = 'none';
    document.getElementById('id04').style.display = 'none';

  }

  reSetPassWord(){
    if(this.password== '' || this.repit_password == ''){
      this.emptyClass = 'error-text1';
    }
    else if(this.password.length <6){
      this.passwordClass = 'error-text1';
      this.emptyClass = 'error-text2';
      this.repit_passClass = 'error-text2';
    }
    else if(this.password != this.repit_password){
      this.passwordClass = 'error-text2';
      this.emptyClass = 'error-text2';
      this.repit_passClass = 'error-text1';
    }
    else{
      this.passwordClass = 'error-text2';
      this.emptyClass = 'error-text2';
      this.repit_passClass = 'error-text2';
    }
  }

  logOut(){
    this.service.logout();
    location.reload();
  }
}
