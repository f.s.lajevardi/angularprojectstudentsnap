import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import user from "../Moudels/user";
import {retry, tap} from "rxjs/operators";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  new_user : user;
  constructor(private httpClient: HttpClient) {
    this.new_user = new user();
  }

  getHeader() :HttpHeaders{
    return new HttpHeaders().set("Authorization", "JWT " + localStorage.getItem('access_token')).
    append('Content-Type', 'application/json');
  }


  register(_username:string, _email:string, _password1:string , _password2: string) {

    let headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    let options = { headers };


    this.new_user.username  = _username;
    this.new_user.email     = _email;
    this.new_user.password1 = _password1;
    this.new_user.password2 = _password2;

    let userjson = JSON.stringify(this.new_user);
    console.log(userjson);

    return this.httpClient.post<{access_token: string}>(environment.register,userjson , options).pipe(retry(3));
  }

  login(usernam: string,  passwor: string) {
    let user = {
      username: usernam,
      password: passwor
    };
    let userjson = JSON.stringify(user);
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'});
    let options = { headers };

    console.log(userjson);
    return this.httpClient.post<any>(environment.login, userjson, options)
      .pipe(tap(res => {
        localStorage.setItem('access_token', res.token);
        console.log(res.token);
      }),retry(3));

  }

  public get loggedIn(): boolean {
    return localStorage.getItem('access_token') !==  null;
  }

  public error_handel( err :HttpErrorResponse){
    if(err.status == 400){
      console.log("error");
    }
  }

  public logout(){
    localStorage.removeItem('access_token');
  }
}
