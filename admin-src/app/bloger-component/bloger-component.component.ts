import { Component, OnInit } from '@angular/core';
import {BlogerService} from "../services/bloger.service";
import blog from "../Moudels/blog";

@Component({
  selector: 'app-bloger-component',
  templateUrl: './bloger-component.component.html',
  styleUrls: ['./bloger-component.component.css']
})
export class BlogerComponentComponent implements OnInit {

  newBlog: blog;
  blogList;
  constructor(private service: BlogerService) {
    this.blogList = new Array<any>();
    this.newBlog = new blog();
  }

  createEntry(){
    this.service.createEntry(this.newBlog).subscribe(res=> {
      console.log(res);
      location.reload();
    },
      error => {
      window.alert("خطا در ایجاد بلاگ جدید");
      })
  }

  deleteEntry(id){
    this.service.deleteEntry(id).subscribe(res=>{
      location.reload();
      },
      error => {
      window.alert('خطا در حذف بلاگ')
      })
  }

  ngOnInit() {
    this.service.getEntry().subscribe((res:any) => {
      this.blogList = res.entries;
      console.log(res)
    },
      error => {
      window.alert("خطا در نمایش بلاگ ها")
      })
  }

}
