import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, ValidatorFn } from '@angular/forms';
import product from "../Moudels/product";
import attribute from "../Moudels/attribute";
import discount from "../Moudels/discount";
import {SellerService} from "../services/seller.service";
import {HttpErrorResponse} from "@angular/common/http";
import category from "../Moudels/category";
import {ManagerService} from "../services/manager.service";

@Component({
  selector: 'app-product-component',
  templateUrl: './product-component.component.html',
  styleUrls: ['./product-component.component.css']
})
export class ProductComponentComponent implements OnInit {

  form: FormGroup;
  newProduct: product;
  ordersData :Array<attribute>;
  selectedAttribute: Array<any>;
  selectedFile:File = null;
  catList: Array<category>;

  constructor(private formBuilder: FormBuilder , private service :SellerService , private manager: ManagerService) {
    this.newProduct = new product();
    this.newProduct.discount = new discount();
    this.ordersData = new Array<attribute>();
    this.selectedAttribute = new Array<any>();
    this.catList = new Array<category>();
  }

  ngOnInit() {
    this.getAttribute();
    this.getCategory();
    this.form = this.formBuilder.group({
      name: this.formBuilder.array([])
    });
  }

  submit() {
    console.log(this.form.value.name);
  }

  onChenge(name: string , isChecked: boolean , index: number) {
    const datas = this.form.controls.name as FormArray;
    this.ordersData[index].isChecked = !(this.ordersData[index].isChecked);
    if(isChecked) {
      datas.push(new FormControl(name));
    } else {
      const index = datas.controls.findIndex(x => x.value === name);
      datas.removeAt(index);
    }
  }

  createNewProduct(){
    for(let i:number = 0; i<this.ordersData.length ; i++){
      if(this.ordersData[i].isChecked){
        let obj = {
          title: this.ordersData[i].detail,
          name: this.ordersData[i].name,
          unit: "گیلوگرم",
          options: null,
          type: "text",
          order: this.ordersData[i].order
        };
        this.selectedAttribute.push(obj);
      }
    }
    this.newProduct.properties = this.selectedAttribute;
    this.service.createProduct(this.newProduct , this.selectedFile);
  }

  getCategory(){
     this.manager.getCategory().subscribe((res: any) => {
       this.catList = res.data;
     })
  }

  getAttribute() {
    this.service.getAttribute().subscribe((res:any)=>{
        let obj :attribute = new attribute();
        for(let i:number = 0; i< res.count ; i++){
          obj = new attribute();
          obj.name = res.data[i].name;
          obj.isChecked = false;
          obj.order = res.data[i].order;
          obj.detail = res.data[i].title;

          this.ordersData.push(obj);
        }

      },
      (error:HttpErrorResponse) => {
      console.log('error in loading attribute'+error);
      });
  }

  onChangeFile(event){
    this.selectedFile = <File>event.target.files[0];
    console.log(event);
  }
}
