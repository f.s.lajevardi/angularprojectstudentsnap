export default class blog {
  constructor() {
  }
  title: string;
  slug: string;
  description: string;
  available: boolean;
  img_url: string;
}
