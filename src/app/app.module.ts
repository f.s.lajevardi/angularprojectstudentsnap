import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { ProFileComponent } from './pro-file/pro-file.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { JwtModule } from '@auth0/angular-jwt';
import { DetaileComponent } from './detaile/detaile.component';
import {ProductService} from "./service/product.service";
import {BlogService} from "./service/blog.service";
import {BuyService} from "./service/buy.service";
import { CartComponent } from './cart/cart.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {MatTabsModule} from "@angular/material/tabs";
import { MDBBootstrapModule } from 'angular-bootstrap-md';

const appRoutes: Routes = [
  {path: 'home' , component: HomeComponent},
  {path: '' , component: HomeComponent},
  {path: 'profile' , component: ProFileComponent},
  {path: 'detail' , component: DetaileComponent},
  {path: 'cart' , component: CartComponent},
  {path: '**' , component: PageNotFoundComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProFileComponent,
    DetaileComponent,
    CartComponent,
    PageNotFoundComponent
  ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes, {enableTracing: true}),
        BrowserAnimationsModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MDBBootstrapModule,
      FormsModule,
        JwtModule.forRoot({
            config: {
                tokenGetter: function tokenGetter() {
                    return localStorage.getItem('access_token');
                },
                whitelistedDomains: ['localhost:4200'],
                blacklistedRoutes: ['http://localhost:3000/auth/login']
            }
        }),
        HttpClientModule,
        MatTabsModule
    ],
  providers: [
    BlogService,
    ProductService,
    BuyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
