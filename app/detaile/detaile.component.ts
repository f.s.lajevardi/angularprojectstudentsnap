import {Component, OnInit} from '@angular/core';
import product from "../Moudels/product";
import attribute from "../Moudels/attribute";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../service/product.service";
import {BuyService} from "../service/buy.service";
import cart from "../Moudels/cart";
import {HttpErrorResponse} from "@angular/common/http";
import {JwtService} from "../service/jwt.service";

@Component({
  selector: 'app-detaile',
  templateUrl: './detaile.component.html',
  styleUrls: ['./detaile.component.css']
})
export class DetaileComponent implements OnInit {

  selected :product;
  uuid: string;
  number :number;
  price : number;
  buyList : Array<cart>;

  constructor(private router: Router ,private route: ActivatedRoute , private service: ProductService , private buyService: BuyService , private jwtService: JwtService) {
    this.number = 0;
    this.price = 0;
    this.buyList = new Array<cart>();
  }

  updatePrice(){
    if(this.number<0) {
      (<HTMLInputElement> document.getElementById('addToCart')).disabled = true;
      (<HTMLInputElement> document.getElementById('addToCart')).style.backgroundColor = '#4e555b';
    }
    else{
      (<HTMLInputElement> document.getElementById('addToCart')).disabled = false;
      (<HTMLInputElement> document.getElementById('addToCart')).style.backgroundColor = '#00c853';
      this.price = this.number*this.selected.sale_price;
    }
  }

  putToCart(){

   this.buyList.push({
      product_uuid: this.selected.uuid,
      count : this.number
    });
    this.buyService.updateCart(this.buyList).subscribe(res => {
      console.log(res);
    }, (error :HttpErrorResponse) => {
      console.log(error.status);
      this.number = 0;
      this.price = 0;
      window.alert('خطا در ارسال اطلاعات محددا تلاش کنید...!')
    });
  }

  ngOnInit() {

    if(!(this.jwtService.loggedIn)){
      window.alert("برای استفاده از بسته ها ابتدا وارد سایت شوید");
      document.getElementById('addToCart').style.display = 'none';
      document.getElementById('numberOfPack').style.display = 'none';
      document.getElementById('pricePack').style.display = 'none';
    }
    this.route.queryParamMap.
    subscribe(params => {
      this.uuid = params.get('id');
    });

    this.service.getProductById(this.uuid).subscribe( res =>{
      this.selected = res;
    }, (error:HttpErrorResponse) => {
      console.log(error.message);
      //this.router.navigate(['../**']);
    });






    let obj :product = new product();
    let obj2 :attribute = new attribute();
      obj2.name = 'شیر کاکائو';
      obj2.unit_priceSell= 768;
      obj2.type_priceBuy = 9000;
      obj2.options_detail = ' که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک';
  let obj3 :attribute = new attribute();
        obj3.name = 'شیر کاکائو';
        obj3.unit_priceSell= 768;
        obj3.type_priceBuy = 9000;
        obj3.options_detail = ' که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک';

    obj.description = 'سانی که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک برنامه از ';
    obj.sale_price=900;
    obj.uuid = 'ghgfhg';
    obj.is_hot = false;
    obj.is_available = true;
    obj.slug = 'uihiuhyug';
    obj.title= 'پذیرایی دفاع';
    obj.discount = null;
    obj.quantity = 55;
    obj.category = 'kkkkkkk';
    obj.image_url = 'https://mehmano.com/img/uploads/20191102110243_5dbd62533987f.png';
    obj.properties = [obj2 , obj3];
    this.selected = obj;
  }

}
