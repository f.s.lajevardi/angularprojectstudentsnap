export  default class category {
  constructor() {
    this.slug = 'slug';
    this.parent = null;
  }

  title: string;
  slug: string;
  description: string;
  parent: any;
}
