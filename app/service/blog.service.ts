import { Injectable } from '@angular/core';
import {HttpClient , HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http : HttpClient) {}

  getEntries(){
    this.http.get(environment.getEntries).subscribe(
      res =>{
        console.log(res);
      }
    )
  }
}
