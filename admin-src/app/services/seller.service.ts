import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpEventType, HttpResponse} from "@angular/common/http";
import product from "../Moudels/product";
import attribute from "../Moudels/attribute";
import {environment} from "../../environments/environment";
import {retry} from "rxjs/operators";
import {BackendService} from "./backend.service";

@Injectable({
  providedIn: 'root'
})
export class SellerService {

  obj: product;
  productList: Array<product>;
  attributeList: Array<attribute>;

  constructor(private http: HttpClient , private jwt: BackendService) {
    this.obj = new product();
    this.productList = new Array<product>();

  }

  getHeader() :any {
    const headers_object = new HttpHeaders().set("Authorization", "JWT " + localStorage.getItem('access_token')).append('Content-Type', 'application/json');
    return {
      headers: headers_object
    };
  }

  getAttribute(): any{
    return this.http.get(environment.getAttribute , this.jwt.getHeader()).pipe(retry(3));
  }

  deleteAttribute(name: string){
    return this.http.delete(environment.createAttribute+name+'/' , this.jwt.getHeader()).pipe(retry(3));
  }

  createAttribute(obj: attribute){
    let attribute = {
      title: obj.detail,
      name: obj.name,
      unit: "گیلوگرم",
      options: null,
      type: "text",
      order: obj.order
    };

    const jsonAttribute = JSON.stringify(attribute);
    console.log(jsonAttribute);
    return this.http.post(environment.createAttribute ,jsonAttribute, this.jwt.getHeader()).pipe(retry(3));
  }

  getProductList(url:string) : Array<product>{
    const headers_object = new HttpHeaders().set("Authorization", "JWT " + localStorage.getItem('access_token')).append('Content-Type', 'application/json');
    const httpOptions = {
      headers : headers_object
    };

    this.http.get(url,httpOptions).subscribe(res => {
      console.log(res);
    });

    return this.productList;
  }

  createProduct(pro: product , image: File){
    let obj3 =  {
      title: pro.title,
      slug: pro.title,
      sku: pro.title,
      description: pro.description,
      excerpt: pro.title,
      category: {
        title: "خواربار",
        slug: "khar222222",
        description: "تور",
        parent: null
      },
      tags: [],
      is_available: true,
      is_slider: true,
      is_featured: false,
      is_hot: false,
      quantity: pro.quantity,
      purchase_price: 5000.0,
      sale_price: pro.sale_price,
      discount: [],
      properties: pro.properties

    };

    let uuid: string;
    const jsonObj = JSON.stringify(obj3);
    this.http.post(environment.createProduct , jsonObj , this.jwt.getHeader()).subscribe((res:any) => {
      uuid = res.uuid;
      console.log(res);
    });

    const fd = new FormData();
    fd.append('image' , image , image.name);//if this need try just image also
    this.http.post(environment.upLoudImage +'VrQndUP6NUMvdeNENbuUQN/' , fd , this.jwt.getHeader()).subscribe(res => {
      console.log(res);
    })

  }
}

/*
uuid: "XBY6vfoAhMuFwTenoShnP2"
title: "حرم"
slug: "haram"
sku: "23421141"
description: "22222"
excerpt: ";aldjkas;p"
category:
title: "تور"
slug: "tour"
parent: null
__proto__: Object
tags: Array(0)
length: 0
__proto__: Array(0)
image:
__proto__: Object
user: {username: "akbar", email: "akbar@gmail.com", first_name: "", last_name: "", avatar: {…}, …}
date_added: "2020-02-23T21:30:42.002026Z"
date_edited: "2020-02-23T21:30:42.004168Z"
is_available: true
is_slider: true
is_featured: false
is_hot: false
quantity: 30
purchase_price: 5000
sale_price: 50000
discount: []
properties: []
galleries: []
 */
