import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {JwtService} from "./jwt.service";
import blog from "../Moudels/blog";
import {retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http : HttpClient , private jwt: JwtService) {}

  getEntries(){
    let httpOptions = {
      headers: this.jwt.getHeader()
    };
    return this.http.get(environment.getEntries , httpOptions).pipe(retry(3));
  }
}
