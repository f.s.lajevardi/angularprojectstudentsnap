import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BackendService} from "./backend.service";
import {environment} from "../../environments/environment";
import {retry} from "rxjs/operators";
import blog from "../Moudels/blog";

@Injectable({
  providedIn: 'root'
})
export class BlogerService {

  constructor(private http: HttpClient , private jwt: BackendService) {
  }

  getEntry(){
    return this.http.get(environment.getEntry , this.jwt.getHeader()).pipe(retry(3));
  }

  deleteEntry(id){
    return this.http.delete(environment.createEntry+id+'/' , this.jwt.getHeader()).pipe(retry(3));
  }

  createEntry(obj: blog){
    let blog = {
      title: obj.title,
      slug: obj.title,
      description: obj.description,
      tags: [],
      available: false,
      is_slider: false
    };

    let test ={
      title: obj.title,
      slug: obj.title,
      description: obj.description,
      excerpt : obj.title,
      tags: [],
      available: true,
      is_slider: false
    };

    return this.http.post(environment.createEntry , JSON.stringify(test) , this.jwt.getHeader()).pipe(retry(3));
  }
}
