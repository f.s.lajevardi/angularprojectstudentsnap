import { Component, OnInit } from '@angular/core';
import {SellerService} from "../services/seller.service";
import attribute from "../Moudels/attribute";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-attribute-component',
  templateUrl: './attribute-component.component.html',
  styleUrls: ['./attribute-component.component.css']
})
export class AttributeComponentComponent implements OnInit {

  attList: Array<attribute>;
  name: string;
  detail: string;
  order: number;

  constructor(private service :SellerService) {
    this.attList =  new Array<attribute>();
  }

  ngOnInit() {
    this.service.getAttribute().subscribe( (item:any)=> {
        console.log(item);
        let obj :attribute = new attribute();
        for(let i:number = 0; i< item.count ; i++){
          obj = new attribute();
          obj.name = item.data[i].name;
          obj.isChecked = false;
          obj.order = item.data[i].order;
          obj.detail = item.data[i].title;
          this.attList.push(obj);
        }

      },
      (error:HttpErrorResponse) => {
        window.alert('خطا در بارگزاری اجناس')
      });
  }

  createAttribute(){
    let obj = new attribute();
    obj.detail = this.detail;
    obj.name = this.name;
    obj.order = this.order;

    this.service.createAttribute(obj).subscribe(res => {
      console.log(res);
      location.reload();
    },
      error => {
      window.alert("خطا در ایجاد جنس جدید");
      })
  }

  deleteAttribute(obj: attribute){
    this.service.deleteAttribute(obj.name).subscribe(res=>{
      console.log(res);
      location.reload();
    },error => {
      window.alert('خطا در حذف جنس');
    });
  }
}
