import { Component, OnInit } from '@angular/core';
import attribute from "../Moudels/attribute";
import product from "../Moudels/product";
import cart from "../Moudels/cart";
import {BuyService} from "../service/buy.service";
import {ProductService} from "../service/product.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  produc :Array<product>;
  prace: number;
  changedList: Array<cart>;
  cartList: Array<cart>;

  constructor(private router: Router , private buyService: BuyService ,private productService: ProductService) {
    this.changedList = new Array<cart>();
    this.cartList = new Array<cart>();
    this.produc = new Array<product>();
    this.prace =0;
  }

  delete(id: number){

  }

  onChangeNumber(obj: product) {
    document.getElementById('upDateCart').style.display = 'block';
    (<HTMLInputElement> document.getElementById('sendToBank')).disabled = true;
    this.prace = this.prace - obj.sale_price;
    obj.sale_price = obj.quantity * obj.description;
    this.prace = this.prace + obj.sale_price;

    this.changedList.push({
      product_uuid: obj.uuid,
      count: obj.quantity
    });

  }


  upDateCart(){
    this.buyService.updateCart(this.changedList).subscribe(
      res =>{
        console.log(res);

        document.getElementById('upDateCart').style.display = 'none';
        (<HTMLInputElement> document.getElementById('sendToBank')).disabled = false;
      },
      error => {
        window.alert('خطا در بروز رسانی سبد خرید');
      }
    )
  }


  paying(){

  }

  getCartList(){
    this.buyService.getCart().subscribe((res:Array<cart>)=>{
        for(let i:number =0 ; i<res.length ; i++){
          this.productService.getProductById(res[i].product_uuid).subscribe((pro:product)=>{
            pro.quantity = res[i].count;
            pro.sale_price = pro.sale_price * res[i].count;
            this.prace = pro.sale_price;
            pro.description = pro.sale_price;///a value for save unite price
            this.produc.push(pro);
          })

        }
      },
      error => {
        //this.router.navigate(['../**']);
      }
    )
  }
  ngOnInit() {
    document.getElementById('upDateCart').style.display = 'none';








    let obj :product = new product();
    let obj22 :product = new product();
    let obj2 :attribute = new attribute();
    obj2.name = 'شیر کاکائو';
    obj2.unit_priceSell= 768;
    obj2.type_priceBuy = 9000;
    obj2.options_detail = ' که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک';
    let obj3 :attribute = new attribute();
    obj3.name = 'شیر کاکائو';
    obj3.unit_priceSell= 768;
    obj3.type_priceBuy = 9000;
    obj3.options_detail = ' که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک';

    obj.description = 'سانی که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک برنامه از ';
    obj.sale_price=900;
    obj.uuid = 'ghgfhg';
    obj.is_hot = false;
    obj.is_available = true;
    obj.slug = 'uihiuhyug';
    obj.title= 'پذیرایی دفاع';
    obj.discount = null;
    obj.quantity = 55;
    obj.category = 'kkkkkkk';
    obj.image_url = 'https://article.innovadatabase.com/articleimgs/article_images/636995556375716447AdobeStock_76774066.jpeg';
    obj.properties = [obj2 , obj3];

    obj22.description = 'سانی که نقشی در پروژه ندارند، می‌تو کامل دارند؛ چرا که استفاده از گیت، ساب‌ورژن و سرویس‌های مشابه از ملزومات کامپایل کردن یک برنامه از ';
    obj22.sale_price=900;
    obj22.uuid = 'ghgfhg';
    obj22.is_hot = false;
    obj22.is_available = true;
    obj22.slug = 'uihiuhyug';
    obj22.title= ' دفاع';
    obj22.discount = null;
    obj22.quantity = 0;
    obj22.category = 'kkkkkkk';
    obj22.image_url = 'https://article.innovadatabase.com/articleimgs/article_images/636995556375716447AdobeStock_76774066.jpeg';
    obj22.properties = [obj2 , obj3];
    this.produc.push(obj);
    this.produc.push(obj);
    this.produc.push(obj);
    this.produc.push(obj22);
  }
}
