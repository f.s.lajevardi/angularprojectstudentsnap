import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import newsList from './NewsServise';
import news from './news';
import product from "../Moudels/product";
import {ProductService} from "../service/product.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None

})
export class HomeComponent implements OnInit {
  list: Array<news>;
  sList: Array<product>;
  index: number;

  constructor(private router: Router ,private service: ProductService) {}


  ngOnInit() {
    const obj = new newsList();
    this.list = obj.getList();
    this.service.getProduct().subscribe( res => {
      this.sList = res;
    },
      error => {
       // this.router.navigate(['../**']);
        console.log(error);
      });
    this.sList = this.service.fake();
  }
}
