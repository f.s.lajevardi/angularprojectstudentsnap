import { Injectable } from '@angular/core';
import {HttpHeaders, HttpClient, HttpResponse} from "@angular/common/http";
import user from "../Moudels/user";
import {environment} from "../../environments/environment";
import {BackendService} from "./backend.service";
import {retry} from "rxjs/operators";
import category from "../Moudels/category";
import discount from "../Moudels/discount";

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  obj : user;
  userList : Array<user>;

  constructor(private httpClient: HttpClient , private backendService: BackendService) {
    this.obj = new user();
    this.userList = new Array<user>();
  }

  public getDiscount(){
    return this.httpClient.get(environment.getDiscount , this.backendService.getHeader()).pipe(retry(3));
  }

  public createDiscount(newDis: discount){
    console.log(JSON.stringify(newDis));
    let obj = {
      title: newDis.title,
      group: {
        name: newDis.title
      },
      percent: newDis.percent,
      is_public_title: newDis.is_public_title,
      start_date: newDis.start_date+"T13:54:24Z",
      end_date: newDis.end_date+"T13:54:24Z"
    };
    return this.httpClient.post(environment.createDiscount , JSON.stringify(obj) , this.backendService.getHeader()).pipe(retry(3));
  }

  public deleteDiscount(uuid: string){
    return this.httpClient.delete(environment.createCategory+uuid , this.backendService.getHeader()).pipe(retry(3));
  }

  public getCategory() {
    return this.httpClient.get(environment.getCategory , this.backendService.getHeader()).pipe(retry(3));
  }

  public deleteCategory(uuid: string) {
    return this.httpClient.delete(environment.createCategory+uuid , this.backendService.getHeader()).pipe(retry(3));
  }

  public createCategory(cat: category){
    let jsonObj = JSON.stringify(cat);
    let obj = {
      title: cat.title,
      slug: cat.title,
      description: cat.description,
      parent: null
    };

    console.log(jsonObj);
    return  this.httpClient.post(environment.createCategory , JSON.stringify(obj) , this.backendService.getHeader()).pipe(retry(3));
  }
  public get_userList(url) :Array<user>{
    const headers_object = new HttpHeaders().set("Authorization", "JWT " + localStorage.getItem('access_token')).append('Content-Type', 'application/json');
    const httpOptions = {
      headers : headers_object
    };

    this.httpClient.get(url , httpOptions).subscribe((res:any) => {

      console.log(res.users);

      for(var i: number =0; i< res.users.length ; i++){
        this.obj = new user();
        this.obj.username = res.users[i].username;
        this.obj.email = res.users[i].email;

        if(res.users[i].profile != null) {
          this.obj.gender = res.users[i].profile.gender;
          this.obj.date_birth = res.users[i].profile.date_birth;
          this.obj.mobile_number = res.users[i].profile.mobile_number;
          this.obj.national_code = res.users[i].profile.national_code;
          this.obj.first_name = res.users[i].first_name;
          this.obj.last_name = res.users[i].last_name;
          this.obj.group = res.users[i].profile.description;
        }

        else {
          this.obj.gender = 'ثبت نشده';
          this.obj.date_birth =  'ثبت نشده';
          this.obj.mobile_number =  'ثبت نشده';
          this.obj.national_code =  'ثبت نشده';
          this.obj.first_name = 'ثبت نشده';
          this.obj.last_name = 'ثبت نشده';
          this.obj.group = 'ثبت نشده';
        }

        this.userList.push(this.obj);
      }
      console.log(this.userList);
    });
    return this.userList;
  }

  public updateUser(newUser: user):any {

    if(newUser.gender == 'ثنت نشده'){
      newUser.gender = 'male';
    }

    if(newUser.date_birth == 'YYYY-MM-DD' || newUser.date_birth == 'ثنت نشده'){
      newUser.date_birth = '0001-01-01';
    }
    const updatedUser = {
      username: newUser.username,
      email: newUser.email,
      first_name: newUser.first_name,
      last_name: newUser.last_name,
      profile: {
        gender: newUser.gender,
        national_code: newUser.national_code,
        mobile_number: newUser.mobile_number,
        date_birth: newUser.date_birth,
        description: newUser.group
      }
    };
    let updatedUserJson = JSON.stringify(updatedUser);
    let httpOptions = {
      headers: this.backendService.getHeader()
    };
    return  this.httpClient.put(environment.updateUser+newUser.username+'/' , updatedUserJson, httpOptions).pipe(retry(3));
  }
}
