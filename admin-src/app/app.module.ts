import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material/icon';
import { ProFileComponent } from './pro-file/pro-file.component';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { ManagerComponentComponent } from './manager-component/manager-component.component';
import { SellerComponentComponent } from './seller-component/seller-component.component';
import { BlogerComponentComponent } from './bloger-component/bloger-component.component';
import { JwtModule } from '@auth0/angular-jwt';
import {BackendService} from "./services/backend.service";
import {ManagerService} from "./services/manager.service";
import {SellerService} from "./services/seller.service";
import {BlogerService} from "./services/bloger.service";
import { AttributeComponentComponent } from './attribute-component/attribute-component.component';
import { ProductComponentComponent } from './product-component/product-component.component';
import { CategoryComponent } from './category/category.component';
import { DiscountComponent } from './discount/discount.component';

const appRoutes: Routes = [
  {path: 'home' , component: HomeComponent},
  {path: 'profile' , component: ProFileComponent},
  {path: 'manager' , component: ManagerComponentComponent},
  {path: 'bloger' , component: BlogerComponentComponent},
  {path: 'seller' , component: SellerComponentComponent},
  {path: 'attribute' , component: AttributeComponentComponent},
  {path: 'product' , component: ProductComponentComponent},
  {path: 'category' , component: CategoryComponent},
  {path: 'discount' , component: DiscountComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProFileComponent,
    ManagerComponentComponent,
    SellerComponentComponent,
    BlogerComponentComponent,
    AttributeComponentComponent,
    ProductComponentComponent,
    CategoryComponent,
    DiscountComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, {enableTracing: true}),
    BrowserAnimationsModule,
    MatIconModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: function  tokenGetter() {
          return     localStorage.getItem('access_token');},
        whitelistedDomains: ['localhost:4200'],
        blacklistedRoutes: ['http://localhost:3000/auth/login']
      }
    }),
    HttpClientModule
  ],
  providers: [
    BackendService,
    ManagerService,
    SellerService,
    BlogerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
