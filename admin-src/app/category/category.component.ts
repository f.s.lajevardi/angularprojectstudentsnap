import { Component, OnInit } from '@angular/core';
import category from "../Moudels/category";
import {ManagerService} from "../services/manager.service";

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  cat :category;
  catList :Array<category>;
  constructor(private service: ManagerService) {
    this.cat = new category();
    this.catList = new Array<category>();
  }

  ngOnInit() {
    this.getCategory();
  }

  createCategory(){
    this.service.createCategory(this.cat).subscribe(
      res=>{
        console.log(res);
        location.reload();
      },
      error =>{
        window.alert('گروه مشابه قبلا ثبت شده است');
        console.log(error);
      }
    );
  }

  getCategory(){
    this.service.getCategory().subscribe((res:any)=> {
      this.catList = res.data;
      console.log(this.catList);
    }, error =>{
      console.log(error);
      window.alert('خطا در مشاهده گروه ها');
    });
  }

  deleteCategory(cat :any){
    this.service.deleteCategory(cat.uuid).subscribe(res=>{
      console.log(res);
      location.reload();
      },
      error => {
      window.alert('خطا در حذف گروه');
      })
  }

}
