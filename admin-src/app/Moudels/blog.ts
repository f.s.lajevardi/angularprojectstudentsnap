export default class blog {
  constructor() {
    this.is_slider = false;
    this.tags = [];
  }
  title: string;
  slug: string;
  description: string;
  tags: any[];
  available: boolean;
  is_slider: boolean;
}
